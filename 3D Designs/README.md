## Sources
- Reaction Wheel by [ReM RC](https://www.thingiverse.com/remrc/designs) [here](https://www.thingiverse.com/thing:5364331)
- Cubesat Frame by [Juliano I.](https://www.thingiverse.com/Juliano85) [here](https://www.thingiverse.com/thing:4096437/files)
- All the other objects are designed by [AcubeSAT](https://gitlab.com/acubesat)
