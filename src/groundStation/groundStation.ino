#include <SPI.h>            
#include <nRF24L01.h>       
#include <RF24.h>    
#include <stdint.h>

RF24 radio(9,10);            
const byte addresses[][6] = {"0000X", "0000Y"}; //ADD the addresses of your team!

enum TC : uint16_t {
  temperatureRequest          =  0x10,
  humidityRequest             =  0x20,
  pressureRequest             =  0x30,
  altitudeValueRequest        =  0x40,
  lightResistorValueRequest   =  0x50,
  motorTurnLeftRequest500ms   =  0x61,
  motorTurnRightRequest500ms  =  0x62,
  motorTurnLeftRequest        =  0x63,
  motorTurnRightRequest       =  0x64,
  motorStopRequest            =  0x65,
  accelerationXRequest        =  0x71,
  accelerationYRequest        =  0x72,
  accelerationZRequest        =  0x73,
  gyroXRequest                =  0x74,
  gyroYRequest                =  0x75,
  gyroZRequest                =  0x76,
};

void menu() {
  Serial.println("!! Welcome to Acubesat SFHMMY15 workshop !!");
  Serial.println();
  Serial.println("* * * * * * * * * Satellite Operations Menu * * * * * * * * *");
  Serial.println();
  Serial.println("Telecommand \"0x10\" by pressing \"1\": get temperature");
  Serial.println("Telecommand \"0x20\" by pressing \"2\": get humidity");
  Serial.println("Telecommand \"0x30\" by pressing \"3\": get pressure");
  Serial.println("Telecommand \"0x40\" by pressing \"4\": get altitude");
  Serial.println("Telecommand \"0x50\" by pressing \"5\": get the light resistor value");
  Serial.println("Telecommand \"0x61\" by pressing \"6\": turn motor left for 500ms");
  Serial.println("Telecommand \"0x62\" by pressing \"7\": turn motor right for 500ms");
  Serial.println("Telecommand \"0x63\" by pressing \"8\": turn motor left");
  Serial.println("Telecommand \"0x64\" by pressing \"9\": turn motor right");
  Serial.println("Telecommand \"0x65\" by pressing \"0\": stop motor");
  Serial.println("Telecommand \"0x71\" by pressing \"q\": get Acceleration X");
  Serial.println("Telecommand \"0x72\" by pressing \"w\": get Acceleration Y");
  Serial.println("Telecommand \"0x73\" by pressing \"e\": get Acceleration Z");
  Serial.println("Telecommand \"0x74\" by pressing \"a\": get Gyro X");
  Serial.println("Telecommand \"0x75\" by pressing \"s\": get Gyro Y");
  Serial.println("Telecommand \"0x76\" by pressing \"d\": get Gyro Z");
  Serial.println();
}

void setup() {
  Serial.begin(115200);           
  radio.begin();
  radio.openWritingPipe(addresses[1]);
  radio.openReadingPipe(1, addresses[0]);
  menu();
}

void loop() {
    char command = '\0'; 
    command = Serial.read();

    uint16_t  telecommand  =  0xFFFF;
    uint32_t  now          =  0;
    uint32_t  current      =  0;
    
    switch ( command ) {
        case '1':
            radio.stopListening();     
            telecommand = temperatureRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;
            
        case '2':
            radio.stopListening();     
            telecommand = humidityRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;
                      
        case '3':
            radio.stopListening();     
            telecommand = pressureRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;
            
        case '4':
            radio.stopListening();     
            telecommand = altitudeValueRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;
            
        case '5':
            radio.stopListening();     
            telecommand = lightResistorValueRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;
            
        case '6':
            radio.stopListening();     
            telecommand = motorTurnLeftRequest500ms;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;
            
        case '7':
            radio.stopListening();     
            telecommand = motorTurnRightRequest500ms;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;
            
        case '8':
            radio.stopListening();     
            telecommand = motorTurnLeftRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;
            
        case '9':
            radio.stopListening();     
            telecommand = motorTurnRightRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;
            
        case '0':
            radio.stopListening();     
            telecommand = motorStopRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;
            
        case 'q':
            radio.stopListening();     
            telecommand = accelerationXRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;

        case 'w':
            radio.stopListening();     
            telecommand = accelerationYRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;

        case 'e':
            radio.stopListening();     
            telecommand = accelerationZRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;

        case 'a':
            radio.stopListening();     
            telecommand = gyroXRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;

        case 's':
            radio.stopListening();     
            telecommand = gyroYRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;

        case 'd':
            radio.stopListening();     
            telecommand = gyroZRequest;
            
            now     = millis();
            current = 0;
            while ( !radio.write(&telecommand, sizeof(telecommand)) && current <= 1000) {
              current = millis() - now;
            }
            break;
    }

    radio.startListening();
    if(radio.available()){
      float data;
      radio.read(&data, sizeof(data));
      Serial.print("Response = ");
      Serial.println(data);
      Serial.println();
    }
}
