#include "SerialTransfer.h"
#include <math.h>
#include <stdio.h>
#include <Adafruit_BMP085.h>
#include <Adafruit_MPU6050.h>
#include "DHT.h"

// Barometric pressure and altitude sensor:
Adafruit_BMP085 bmp;

// Digital Humidity and Temperature sensor:
DHT dht;

// Accelerometer and gyroscope sensor:
Adafruit_MPU6050 mpu;

// Transferring data between devices using serial communication:
SerialTransfer dataTransfer;

// Define pins for motor driver connections:
const int IA1 = 8; // or 6
const int IB1 = 9; // or 7

enum TC : uint16_t {
  temperatureRequest          =  0x10,
  humidityRequest             =  0x20,
  pressureRequest             =  0x30,
  altitudeRequest             =  0x40,
  lightResistorValueRequest   =  0x50,
  motorTurnLeftRequest500ms   =  0x61,
  motorTurnRightRequest500ms  =  0x62,
  motorTurnLeftRequest        =  0x63,
  motorTurnRightRequest       =  0x64,
  motorStopRequest            =  0x65,
  accelerationXRequest        =  0x71,
  accelerationYRequest        =  0x72,
  accelerationZRequest        =  0x73,
  gyroXRequest                =  0x74,
  gyroYRequest                =  0x75,
  gyroZRequest                =  0x76,
};

struct SensorBoardMeasurements {
  float   temperature          =  0.;
  float   humidity             =  0.;
  float   pressure             =  0.;
  float   altitude        =  0.;
  float   lightResistorValue   =  0.;
  float   accelX               =  0.;
  float   accelY               =  0.;
  float   accelZ               =  0.;
  float   gyroX                =  0.;
  float   gyroY                =  0.;
  float   gyroZ                =  0.;
} sensorBoardMeasurements;

void transferFloatData(float data) {
  dataTransfer.sendData( dataTransfer.txObj(data, 0) );
}

void getTemperature();
void getHumidity();
void getPressure();
void getAltitude();
void getLightResistor();
void turnMotorRight();
void turnMotorLeft();
void stopMotor();
void getAccelX();
void getAccelY();
void getAccelZ();
void getGyroX();
void getGyroY();
void getGyroZ();


void setup() {
  Serial.begin(115200);
  dataTransfer.begin(Serial);
  bmp.begin();
  dht.setup(2);
  mpu.begin();
  
  pinMode(IA1, OUTPUT);
  pinMode(IB1, OUTPUT);
  digitalWrite(IA1, HIGH);
  digitalWrite(IB1, HIGH);
}

void loop() {
  uint16_t recSize = 0;
  uint16_t request = 0xFFFF;

//  getTemperature();
Serial.println(sensorBoardMeasurements.temperature);

  if(dataTransfer.available()){
    recSize = dataTransfer.rxObj(request, recSize);

    switch( request ) {
      case temperatureRequest:          getTemperature();    break;
      case humidityRequest:             getHumidity();       break;
      case pressureRequest:             getPressure();       break;
      case altitudeRequest:        getAltitude();       break;
      case lightResistorValueRequest:   getLightResistor();  break;
      case motorTurnLeftRequest500ms:   turnMotorLeft();    delay(6000);  stopMotor(); break;
      case motorTurnRightRequest500ms:  turnMotorRight();   delay(6000);  stopMotor(); break;
      case motorTurnLeftRequest:        turnMotorLeft();   break;
      case motorTurnRightRequest:       turnMotorRight();  break;
      case motorStopRequest:            stopMotor();       break;
      case accelerationXRequest:        getAccelX();       break;
      case accelerationYRequest:        getAccelY();       break;
      case accelerationZRequest:        getAccelZ();       break;
      case gyroXRequest:                getGyroX();        break;
      case gyroYRequest:                getGyroY();        break;
      case gyroZRequest:                getGyroZ();        break;

    }
  }

}


void getTemperature() {
  transferFloatData(
    sensorBoardMeasurements.temperature = dht.getTemperature()
    );
}

void getHumidity(){
  transferFloatData(
    sensorBoardMeasurements.humidity = dht.getHumidity()
    );
}

void getPressure(){
  transferFloatData(
    sensorBoardMeasurements.pressure = bmp.readPressure()
    );
}

void getAltitude(){
  transferFloatData(
    sensorBoardMeasurements.altitude = bmp.readAltitude()
    );
}

void getLightResistor(){
  transferFloatData(
    sensorBoardMeasurements.lightResistorValue = digitalRead(3)
    );
}

void turnMotorRight() { digitalWrite(IA1,HIGH); digitalWrite(IB1,LOW);  }
void turnMotorLeft()  { digitalWrite(IA1,LOW);  digitalWrite(IB1,HIGH); }
void stopMotor()      { digitalWrite(IA1,LOW);  digitalWrite(IB1,LOW);  }

void getAccelX() {
  sensors_event_t a, g, tmp;
  mpu.getEvent(&a, &g, &tmp);
  transferFloatData(
    sensorBoardMeasurements.accelX = a.acceleration.x
    );
}

void getAccelY() {
  sensors_event_t a, g, tmp;
  mpu.getEvent(&a, &g, &tmp);
  transferFloatData(
    sensorBoardMeasurements.accelY = a.acceleration.y
    );
}

void getAccelZ() {
  sensors_event_t a, g, tmp;
  mpu.getEvent(&a, &g, &tmp);
  transferFloatData(
    sensorBoardMeasurements.accelZ = a.acceleration.z
    );
}

void getGyroX() {
  sensors_event_t a, g, tmp;
  mpu.getEvent(&a, &g, &tmp);
  transferFloatData(
    sensorBoardMeasurements.gyroX = g.gyro.x
    );
}

void getGyroY() {
  sensors_event_t a, g, tmp;
  mpu.getEvent(&a, &g, &tmp);
  transferFloatData(
    sensorBoardMeasurements.gyroY = g.gyro.y
    );
}

void getGyroZ() {
  sensors_event_t a, g, tmp;
  mpu.getEvent(&a, &g, &tmp);
  transferFloatData(
    sensorBoardMeasurements.gyroZ = g.gyro.z
    );
}
