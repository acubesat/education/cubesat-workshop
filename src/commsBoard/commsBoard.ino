#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include "SerialTransfer.h"

SerialTransfer dataTransfer;

RF24 radio(7, 8);
const byte addresses[][6] = {"0000X", "0000Y"}; //ADD the addresses of your team!

enum TC : uint16_t {
  temperatureRequest          =  0x10,
  humidityRequest             =  0x20,
  pressureRequest             =  0x30,
  altitudeValueRequest        =  0x40,
  lightResistorValueRequest   =  0x50,
  motorTurnLeftRequest500ms   =  0x61,
  motorTurnRightRequest500ms  =  0x62,
  motorTurnLeftRequest        =  0x63,
  motorTurnRightRequest       =  0x64,
  motorStopRequest            =  0x65,
  accelerationXRequest        =  0x71,
  accelerationYRequest        =  0x72,
  accelerationZRequest        =  0x73,
  gyroXRequest                =  0x74,
  gyroYRequest                =  0x75,
  gyroZRequest                =  0x76,
};


void setup() {
  Serial.begin(115200);
  dataTransfer.begin(Serial);
  radio.begin();

  radio.openWritingPipe(addresses[0]);
  radio.openReadingPipe(1, addresses[1]);

  radio.setPALevel(RF24_PA_MIN);
}

void loop() {

  uint16_t  recSize   =  0;
  uint16_t  sendSize  =  0;
  
  uint16_t  request   =  0xFFFF;
  float     data      =  0.;

  radio.startListening();
  if (radio.available()) {
    radio.read(&request, sizeof(request));

    sendSize = dataTransfer.txObj(request, sendSize);
    dataTransfer.sendData(sendSize);
  }

  if (dataTransfer.available()) {
  recSize = dataTransfer.rxObj(data, recSize);
  Serial.println(data);
    radio.stopListening();
    uint32_t now     = millis();
    uint32_t current = 0;
    while ( !radio.write(&data, sizeof(data)) && current <= 1000) {
      current = millis() - now;
    }
  }
}
